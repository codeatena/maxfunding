package com.example.maxfunding;

public class PersonalInfo {
	
	public int countMotorVehicle = 0;
	public int countRealState = 0;
	
	public String strLoanAmount = "";
	public String strTitle1 = "";
	public String strFirstName1 = "";
	public String strMiddleName1 = "";
	public String strSurname1 = "";
	public String strResidency = "";
	public String strDOB = "";
	public String strDriversLicense1 = ""; 
	public String strMobilePhone1 = "";
	public String strHomePhone1 = "";
	public String strEMailAddress = "";
	public String strResidentialStatus = "";
	
	public String strResidentialAddress1 = "";
	public String strSuburb1 = "";
	public String strState1 = "";
	public String strPostcode1 = "";
	
	public String strNumberOfDependant1 = "";
	public String strPurposeOfLoan1 = "";
	public String strOtherPurpose = "";
	public String strPurposeSummary = "";
	
	// Vehicle1
	public String strVehcieValue1 = "";
	public String strMotorMake1 = "";
	public String strMotorModel1 = "";
	public String strVehicleOwner1 = "";
	public String strMotorFinance1 = "";
	public String strMotorYear1 = "";
	public String strInsurance1 = "";
	
	// Vehicle2
	public String strVehcieValue2 = "";
	public String strMotorMake2 = "";
	public String strMotorModel2 = "";
	public String strVehicleOwner2 = "";
	public String strMotorFinance2 = "";
	public String strMotorYear2 = "";
	public String strInsurance2 = "";
	
	//PropertyValue1
	public String strMarketValue1 = "";
	public String strPropertyAddress1 = "";
	public String strMorarr1 = "";
	public String strPropertyOwner1 = "";
	public String strEstateType1 = "";
	public String strEstateDescription1 = "";
	public String strLandSize1 = "";
	public String strBedrooms1 = "";
	public String strBathrooms1 = "";
	
	//PropertyValue2
	public String strMarketValue2 = "";
	public String strPropertyAddress2 = "";
	public String strMorarr2 = "";
	public String strPropertyOwner2 = "";
	public String strEstateType2 = "";
	public String strEstateDescription2 = "";
	public String strLandSize2 = "";
	public String strBedrooms2 = "";
	public String strBathrooms2 = "";
	
	//Liabilities @ Empense
	
	public String strBoardingRentingCost = "";
	public String strLivingExpensesPayment = "";
	
	/////Mortgage1
	public String strOwingCreditLimit1 = "";
	public String strMothlyPayment1 = "";
	public String strLender1 = "";
	
	/////Mortgage2
	public String strOwingCreditLimit2 = "";
	public String strMothlyPayment2 = "";
	public String strLender2 = "";
	
	////PersonalLoan
	public String strLoanType = "";
	public String strPersonalLoanOwing = "";
	public String strPersonalLoanPayment = "";
	public String strPersonalLoanLender = "";
	
	public String strBusinessLoanOwing = "";
	public String strBusinessLoanLender = "";
	public String strCreditCardOwing = "";
	public String strCreditCardLender = "";
	
	////Vehicle1
	public String strVehicleOwing1 = "";
	public String strVehiclePayment1 = "";
	public String strVehicleLender1 = "";
	
	////Vehicle2
	public String strVehicleOwing2 = "";
	public String strVehiclePayment2 = "";
	public String strVehicleLender2 = "";
	
	////Employment or Company Detail
	public String strEmploymentStatus = "";
	public String strEmploymentType = "";
	public String strTitleOccupation = "";
	public String strEmployerName = "";
	public String strHrContactNumber1 = "";
	
	////Business
	public String strStageBusiness 			= "";
	public String strTypeEntitiy 			= "";
	public String strRelationShip1			= "";
	public String strRelationShip2			= "";
	public String strBusinessName 			= "";
	public String strABNNumber 				= "";
	public String strNatureBusiness 		= "";
	public String strBusinessAddress 		= "";
	public String strCompanyWebsite 		= "";
	public String strGiveMoneyDetail 		= "";
	public String strNameDirectors			= "";
	public String strMostLikeRepay 			= "";
	public String strMostLikeRepayPeriodly 	= "";
	public String strRepayLoanBy 			= "";
	public String strStagePropertySale		= "";
	public String strStageRefinancing		= "";
	public String strOtherSpecify			= "";
	public String strBeneficiary			= "";
	public String strBusinessIncome			= "";

	////Income Details
	public String strIncomeAfterTax = "";
	public String strIncomeAfterTaxPeriod = "";
	public String strCenterLinkCom = "";
	public String strOtherIncomeSpecify = "";
	public String strRentalIncome = "";
	public String strOtherIncomePeriod = "";
	
	////Other Information and Credit
	public String strNamePartner = "";
	public String strContactNumber = "";
	public String strCreditFiveYears = "";
	public String strHardShip1 = "";
	public String strCreditHistory = "";
	public String strTotalOutstanding = "";
	public String strDischargedDate = "";
	public String strDefaultAfterDischage = "";
	public String strExistingClient = "";
	
	public String strAppID = "10001";
}
