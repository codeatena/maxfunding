package com.morganfinance.submit;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.example.maxfunding.R;

public class RepayLoanDialog extends Dialog {

	CheckBox chkBusinessCashFlow;
	CheckBox chkWageSalary;
	CheckBox chkInvestmentProfit;
	CheckBox chkCentrelinkIncome;
	CheckBox chkSaleRealEstateProperty;
	CheckBox chkSaleVehicleOtherProperty;
	CheckBox chkRefinancing;
	CheckBox chkOthers;
	
	Button btnCancel;
	public static int checkGroupValue = 128;
	
	public RepayLoanDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.setContentView(com.example.maxfunding.R.layout.repay_loan_by_dialog);
		setCanceledOnTouchOutside(false);
		setCancelable(false);
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		chkBusinessCashFlow 		= (CheckBox) findViewById(R.id.business_cash_flow_checkbox);
		chkWageSalary 				= (CheckBox) findViewById(R.id.wage_salary_checkbox);
		chkInvestmentProfit 		= (CheckBox) findViewById(R.id.investment_profit_checkbox);
		chkCentrelinkIncome 		= (CheckBox) findViewById(R.id.centerlink_income_checkbox);
		chkSaleRealEstateProperty 	= (CheckBox) findViewById(R.id.sale_real_estate_property_checkbox);
		chkSaleVehicleOtherProperty = (CheckBox) findViewById(R.id.sale_vehicle_other_property_checkbox);
		chkRefinancing 				= (CheckBox) findViewById(R.id.refinancing_checkbox);
		chkOthers 					= (CheckBox) findViewById(R.id.others_checkbox);
		btnCancel 					= (Button)   findViewById(R.id.cancel_button);
	}
	
	private void initValue() {
		
	}
	
	public void setGroupValue() {
		int k = checkGroupValue;

		chkBusinessCashFlow.setChecked(false);
		chkWageSalary.setChecked(false);
		chkInvestmentProfit.setChecked(false);
		chkCentrelinkIncome.setChecked(false);
		chkSaleRealEstateProperty.setChecked(false);
		chkSaleVehicleOtherProperty.setChecked(false);		
		chkRefinancing.setChecked(false);
		chkOthers.setChecked(false);

		String str = "";

		if (k >= 128) {
			chkBusinessCashFlow.setChecked(true);
			str = str + ", " + chkBusinessCashFlow.getText().toString();
			k -= 128;
		}
		if (k >= 64) {
			chkWageSalary.setChecked(true);
			str = str + ", " + chkWageSalary.getText().toString();
			k -= 64;
		}
		if (k >= 32) {
			chkInvestmentProfit.setChecked(true);
			str = str + ", " + chkInvestmentProfit.getText().toString();
			k -= 32;
		}
		if (k >= 16) {
			chkCentrelinkIncome.setChecked(true);
			str = str + ", " + chkCentrelinkIncome.getText().toString();
			k -= 16;
		}
		if (k >= 8) {
			chkSaleRealEstateProperty.setChecked(true);
			str = str + ", " + chkSaleRealEstateProperty.getText().toString();
			k -= 8;
		}
		if (k >= 4) {
			chkSaleVehicleOtherProperty.setChecked(true);
			str = str + ", " + chkSaleVehicleOtherProperty.getText().toString();
			k -= 4;
		}
		if (k >= 2) {
			chkRefinancing.setChecked(true);
			str = str + ", " + chkRefinancing.getText().toString();
			k -= 2;
		}
		if (k >= 1) {
			chkOthers.setChecked(true);
			str = str + ", " + chkOthers.getText().toString();
		}
		if (!str.equals("")) 
			str = str.substring(2);
		
		BusinessDetail.btnRepayLoanBy.setText(str);
	}
	
	private void initEvent() {
		chkSaleRealEstateProperty.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    BusinessDetail.llytStagePropertySale.setVisibility(View.VISIBLE);
                } else {
                	BusinessDetail.llytStagePropertySale.setVisibility(View.GONE);
                }
            }
        });
		chkRefinancing.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    BusinessDetail.llytStageRefinancing.setVisibility(View.VISIBLE);
                } else {
                	BusinessDetail.llytStageRefinancing.setVisibility(View.GONE);
                }
            }
        });
		chkOthers.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    BusinessDetail.llytOtherSpecify.setVisibility(View.VISIBLE);
                } else {
                	BusinessDetail.llytOtherSpecify.setVisibility(View.GONE);
                }
            }
        });
		btnCancel.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				checkGroupValue = 0;
				String str = "";
				
				if (chkBusinessCashFlow.isChecked()) {
					checkGroupValue += 128;
					str = str + ", " + chkBusinessCashFlow.getText().toString();
				}
				if (chkWageSalary.isChecked()) {
					checkGroupValue += 64;
					str = str + ", " + chkWageSalary.getText().toString();
				}
				if (chkInvestmentProfit.isChecked()) {
					checkGroupValue += 32;
					str = str + ", " + chkInvestmentProfit.getText().toString();
				}
				if (chkCentrelinkIncome.isChecked()) {
					checkGroupValue += 16;
					str = str + ", " + chkCentrelinkIncome.getText().toString();
				}
				if (chkSaleRealEstateProperty.isChecked()) {
					checkGroupValue += 8;
					str = str + ", " + chkSaleRealEstateProperty.getText().toString();
				}
				if (chkSaleVehicleOtherProperty.isChecked()) {
					checkGroupValue += 4;
					str = str + ", " + chkSaleVehicleOtherProperty.getText().toString();
				}
				if (chkRefinancing.isChecked()) {
					checkGroupValue += 2;
					str = str + ", " + chkRefinancing.getText().toString();
				}
				if (chkOthers.isChecked()) {
					checkGroupValue += 1;
					str = str + ", " + chkOthers.getText().toString();
				}
				if (!str.equals("")) str.substring(2);
				
				BusinessDetail.btnRepayLoanBy.setText(str);
				RepayLoanDialog.this.dismiss();
			}
        });
	}
}
