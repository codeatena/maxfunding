package com.morganfinance.submit;

import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.maxfunding.GlobalData;
import com.example.maxfunding.HomeActivity;
import com.example.maxfunding.R;
import com.example.maxfunding.SubmitActivity;

public class BusinessDetail {

	SubmitActivity parent;
	public View view;
	
	public static final int ERROR_NONE = -1;
	public static final int ERROR_GENERAL = 0;
	public static final int ERROR_BUSINESS_NAME = 1;
	public static final int ERROR_ABN_ACN_NUMBER = 2;
	public static final int ERROR_NATURE_BUSINESS = 3;
	public static final int ERROR_DETAIL_ADDRESS = 4;
	public static final int ERROR_NAME_DIRECTORS = 5;
	public static final int ERROR_MOST_LIKE_REPAY = 6;
	public static final int ERROR_REPAY_LOAN_BY = 7;
	public static final int ERROR_STAGE_PROPERTY_SALE = 8;
	public static final int ERROR_STAGE_REFINANCING = 9;
	public static final int ERROR_OTHER_SPECIFY = 10;
	
	public Spinner 	spnStageBusiness;
	public Spinner 	spnTypeEntity;
	public EditText	edtBusinessName;
	public EditText	edtABNNumber;
	public Button		btnABNLookup;
	public EditText	edtNatureBusiness;
	public EditText	edtDetailAddress;
	public EditText 	edtCompanyWebsite;
	public EditText	edtGiveDetails;
	public EditText 	edtNameDirector;
	
	public EditText	edtMostLikeRepay;
	public Spinner		spnMostLikeRepay;
	public static Button		btnRepayLoanBy;
	public Spinner		spnStagePropertySale;
	public Spinner		spnStageRefinancing;
	public EditText	edtOtherSpecify;
	
	public static LinearLayout llytStagePropertySale;
	public static LinearLayout llytStageRefinancing;
	public static LinearLayout llytOtherSpecify;
	
	RepayLoanDialog repayLoanDlg;
	
	public BusinessDetail(Context context) {
		// TODO Auto-generated constructor stub
		parent = (SubmitActivity) context;
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
    	LayoutInflater inflater = parent.getLayoutInflater();
    	view = inflater.inflate(R.layout.business_detail, null);
    	
    	spnStageBusiness = (Spinner) view.findViewById(R.id.stage_business_spinner);
    	spnTypeEntity 	= (Spinner) view.findViewById(R.id.type_entitiy_spinner);
    	edtBusinessName = (EditText) view.findViewById(R.id.business_name_editText);
    	edtABNNumber = (EditText) view.findViewById(R.id.abn_acn_number_editText);
    	btnABNLookup = (Button) view.findViewById(R.id.abn_acn_lookup_button);
    	edtNatureBusiness = (EditText) view.findViewById(R.id.nature_business_editText);
    	edtDetailAddress = (EditText) view.findViewById(R.id.detail_address_editText);
    	edtCompanyWebsite = (EditText) view.findViewById(R.id.company_website_editText);
    	edtGiveDetails = (EditText) view.findViewById(R.id.give_detail_editText);
    	edtNameDirector = (EditText) view.findViewById(R.id.name_director_editText);
    	
    	edtMostLikeRepay = (EditText) view.findViewById(R.id.most_like_repay_editText);
    	spnMostLikeRepay = (Spinner) view.findViewById(R.id.most_like_repay_spinner);
    	btnRepayLoanBy = (Button) view.findViewById(R.id.repay_loan_by_button);
    	spnStagePropertySale = (Spinner) view.findViewById(R.id.stage_property_sale_spinner);
    	spnStageRefinancing = (Spinner) view.findViewById(R.id.stage_refinancing_spinner);
    	edtOtherSpecify = (EditText) view.findViewById(R.id.other_please_specify_editText);
    	
    	llytStagePropertySale = (LinearLayout) view.findViewById(R.id.stage_property_sale_layout);
    	llytStageRefinancing = (LinearLayout) view.findViewById(R.id.stage_refinancing_layout);
    	llytOtherSpecify = (LinearLayout) view.findViewById(R.id.other_specify_layout);
    	
    	repayLoanDlg = new RepayLoanDialog(parent);
	}
	
	private void initValue() {
		ArrayAdapter<String> spinnerArrayAdapter;
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfStageBusiness);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnStageBusiness.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfTypeEntity);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnTypeEntity.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfPeriodly);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnMostLikeRepay.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfStagePropertySale);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnStagePropertySale.setAdapter(spinnerArrayAdapter);
		
		spinnerArrayAdapter = new ArrayAdapter<String>(parent, android.R.layout.simple_spinner_item, GlobalData.itemsOfStageRefinancing);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
		spnStageRefinancing.setAdapter(spinnerArrayAdapter);
		
		loadSavedPreferences();
		repayLoanDlg.setGroupValue();
	}
	
	private void initEvent() {
		btnRepayLoanBy.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				repayLoanDlg.show();
			}  	
        });
	}
	
	private void loadSavedPreferences() {
		SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	
   	 	String str;
        int n;

        n = sharedPreferences.getInt("nStageBusiness", 0);
        spnStageBusiness.setSelection(n);
        n = sharedPreferences.getInt("nTypeEntity", 0);
        spnTypeEntity.setSelection(n);
        str = sharedPreferences.getString("strBusinessName", "");
        edtBusinessName.setText(str);
        str = sharedPreferences.getString("strABNNumber", "");
        edtABNNumber.setText(str);
        str = sharedPreferences.getString("strNatureBusiness", "");
        edtNatureBusiness.setText(str);
        str = sharedPreferences.getString("strDetailAddress", "");
        edtDetailAddress.setText(str);
        str = sharedPreferences.getString("strCompanyWebsite", "");
        edtCompanyWebsite.setText(str);
        str = sharedPreferences.getString("strGiveDetails", "");
        edtGiveDetails.setText(str);
        str = sharedPreferences.getString("strNameDirectors", "");
        edtNameDirector.setText(str);
        
        str = sharedPreferences.getString("strMostLikeRepay", "");
        edtMostLikeRepay.setText(str);
        n = sharedPreferences.getInt("nMostLikeRepay", 0);
        spnMostLikeRepay.setSelection(n);
        str = sharedPreferences.getString("strRepayLoanBy", "");
        btnRepayLoanBy.setText(str);
        n = sharedPreferences.getInt("nStagePropertySale", 0);
        spnStagePropertySale.setSelection(n);
        n = sharedPreferences.getInt("nStageRefinancing", 0);
        spnStageRefinancing.setSelection(n);
        str = sharedPreferences.getString("strOtherSpecify", "");
        edtOtherSpecify.setText(str);
	}
	
    public void savePreferences() {
    	SharedPreferences sharedPreferences = HomeActivity.sharedPreferences;
    	SharedPreferences.Editor editor = sharedPreferences.edit();
    	
    	editor.putInt("nStageBusiness", spnStageBusiness.getSelectedItemPosition());
    	editor.putInt("nTypeEntity", spnTypeEntity.getSelectedItemPosition());
   	 	editor.putString("strBusinessName", edtBusinessName.getText().toString());
   	 	editor.putString("strABNNumber", edtABNNumber.getText().toString());
   	 	editor.putString("strNatureBusiness", edtNatureBusiness.getText().toString());
   	 	editor.putString("strDetailAddress", edtDetailAddress.getText().toString());
   	 	editor.putString("strCompanyWebsite", edtCompanyWebsite.getText().toString());
   	 	editor.putString("strGiveDetails", edtGiveDetails.getText().toString());
   	 	editor.putString("strNameDirectors", edtNameDirector.getText().toString());
   	 	
   	 	editor.putString("strMostLikeRepay", edtMostLikeRepay.getText().toString());
    	editor.putInt("nMostLikeRepay", spnMostLikeRepay.getSelectedItemPosition());
   	 	editor.putString("strRepayLoanBy", btnRepayLoanBy.getText().toString());
   	 	editor.putInt("nStagePropertySale", spnStagePropertySale.getSelectedItemPosition());
   	 	editor.putInt("nStageRefinancing", spnStageRefinancing.getSelectedItemPosition());
   	 	editor.putString("strOtherSpecify", edtOtherSpecify.getText().toString());

    	editor.commit();
    }
    
	public void showGeneralAlert(String title, String message) {
    	new AlertDialog.Builder(parent)
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
                // continue with delete
            }
         })
         .show();
    }
	
	public int checkInput() {
		String str;
		
		str = GlobalData.itemsOfStageBusiness[(int)  spnStageBusiness.getSelectedItemPosition()];
    	if (str.equals("-Select-")) {
    		showGeneralAlert("Select", "Please select a stage business!");
    		return ERROR_GENERAL;
    	}
    	str = GlobalData.itemsOfTypeEntity[(int)  spnTypeEntity.getSelectedItemPosition()];
    	if (str.equals("-Select-")) {
    		showGeneralAlert("Select", "Please select a type of Entitiy!");
    		return ERROR_GENERAL;
    	}
    	str = edtBusinessName.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Business Name!");

    		edtBusinessName.requestFocus();
    		return ERROR_BUSINESS_NAME;
    	}
//    	str = edtABNNumber.getText().toString();
//    	if (str.equals("")) {
//    		showGeneralAlert("Input", "Please input value in Work Income after Tax!");
//
//    		edtABNNumber.requestFocus();
//    		return ERROR_ABN_ACN_NUMBER;
//    	}
    	str = edtNatureBusiness.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Nature of business!");

    		edtNatureBusiness.requestFocus();
    		return ERROR_NATURE_BUSINESS;
    	}
    	str = edtDetailAddress.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input Address!");

    		edtDetailAddress.requestFocus();
    		return ERROR_DETAIL_ADDRESS;
    	}
    	str = edtNameDirector.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input name of Directors!");

    		edtNameDirector.requestFocus();
    		return ERROR_NAME_DIRECTORS;
    	}
    	str = edtMostLikeRepay.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input the most you'd like to repay");
    		
    		edtMostLikeRepay.requestFocus();
    		return ERROR_MOST_LIKE_REPAY;
    	}
//    	str = btnRepayLoanBy.getText().toString();
//    	if (str.equals("")) {
//    		showGeneralAlert("Select", "Please select an repay loan by");
//    		return ERROR_GENERAL;
//    	}
    	str = GlobalData.itemsOfStagePropertySale[(int)  spnStagePropertySale.getSelectedItemPosition()];
    	if (str.equals("-Select-")) {
    		showGeneralAlert("Select", "Please select an Stage of property sale");
    		return ERROR_GENERAL;
    	}
    	str = GlobalData.itemsOfStageRefinancing[(int)  spnStageRefinancing.getSelectedItemPosition()];
    	if (str.equals("-Select-")) {
    		showGeneralAlert("Select", "Please select an Stage of refinancing");
    		return ERROR_GENERAL;
    	}
    	str = edtOtherSpecify.getText().toString();
    	if (str.equals("")) {
    		showGeneralAlert("Input", "Please input other specify");
    		edtOtherSpecify.requestFocus();
    		return ERROR_OTHER_SPECIFY;
    	}
        
    	GlobalData.personalInfo.strStageBusiness = GlobalData.itemsOfStageBusiness[(int)  spnStageBusiness.getSelectedItemPosition()];
    	GlobalData.personalInfo.strTypeEntitiy = GlobalData.itemsOfTypeEntity[(int)  spnTypeEntity.getSelectedItemPosition()];
    	GlobalData.personalInfo.strBusinessName = edtBusinessName.getText().toString();
    	GlobalData.personalInfo.strABNNumber = edtABNNumber.getText().toString();
    	GlobalData.personalInfo.strNatureBusiness = edtNatureBusiness.getText().toString();
    	GlobalData.personalInfo.strBusinessAddress = edtDetailAddress.getText().toString();
    	GlobalData.personalInfo.strCompanyWebsite = edtCompanyWebsite.getText().toString();
    	GlobalData.personalInfo.strGiveMoneyDetail = edtGiveDetails.getText().toString();
    	GlobalData.personalInfo.strNameDirectors = edtNameDirector.getText().toString();
    	GlobalData.personalInfo.strMostLikeRepay = edtMostLikeRepay.getText().toString();
    	GlobalData.personalInfo.strRepayLoanBy = btnRepayLoanBy.getText().toString();
    	
//    	public String strOtherIncomeSpecify = "";
//    	public String strRentalIncome = "";
//    	public String strOtherIncomePeriod = "";
    	
    	return ERROR_NONE;
	}
}
